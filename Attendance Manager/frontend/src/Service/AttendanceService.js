import axios from "axios";

const ATTENDANCE_BASE_REST_API_URL = 'http://localhost:8082/api/v1/attendance';

class AttendanceService {
    
    getAttendances(){
        return axios.get(ATTENDANCE_BASE_REST_API_URL)
    }

    addAttendance(attendance){
        return axios.post(ATTENDANCE_BASE_REST_API_URL, attendance)
    }

    getRecentActivities(){
        return axios.get(ATTENDANCE_BASE_REST_API_URL + '/pageList')
    }

    getWorkDays(startDate, endDate){
        return axios.get(ATTENDANCE_BASE_REST_API_URL + '/countWorkDays'+ startDate, endDate)
    }

    getWorkTime(startDate, endDate){
        return axios.get(ATTENDANCE_BASE_REST_API_URL +'/attendanceAnalysis' + startDate, endDate)
    }

    getAttendanceCalculate(){
        return axios.get(ATTENDANCE_BASE_REST_API_URL + '/calculate')
    }
}

export default new AttendanceService();