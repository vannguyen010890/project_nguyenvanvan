package com.rikkei.service;
import com.rikkei.entity.Attendance;
import com.rikkei.entity.AttendanceCalculate;
import com.rikkei.formRequest.FormDate;
import com.rikkei.repository.AttendanceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private AttendanceRepository attendanceRepository;

    @Override
    public List<Attendance> getAttendances() {
        List<Attendance> getAttendances = attendanceRepository.findAll();
        return getAttendances;
    }

    @Override
    public Attendance getById(Long id) {
        return attendanceRepository.findById(id).get();
    }

    @Override
    public Page<Attendance> findAll(Pageable pageable) {
        Page<Attendance> attendancePage = attendanceRepository.findAll(PageRequest.of(0,6, Sort.by("clockingTime").descending()));
        return attendancePage;
    }

    @Override
    public Attendance addAttendance(Attendance attendance) {

        return attendanceRepository.save(attendance);
    }

    @Override
    public Long countWorkDay(FormDate formDate) {
        return attendanceRepository.countAttendancesByClockingTime(formDate.getStartDate(),formDate.getEndDate());
    }

    @Override
    public List<Attendance> findAttendancesByClockingTime(LocalDate startDate, LocalDate endDate) {
        return attendanceRepository.findAttendancesByClockingTime(startDate,endDate);
    }

    @Override
    public Long attendanceAnalysis(LocalDate startDate, LocalDate endDate) {
        List<Attendance> attendanceList = attendanceRepository.findAttendancesByClockingTime(startDate,endDate);
        Map<String, Attendance> mapBegin = new HashMap<>();
        Map<String, Attendance> mapFinish = new HashMap<>();
        final int breakTime= 60;
        final LocalTime beginTime = LocalTime.of(9,30,0);
        final LocalTime finishTime = LocalTime.of(18,30,0);
        Long totalWorkDuration = 0L;
        for(Attendance attendance: attendanceList){
            if(attendance.getAttendanceStatus().equals("begin")){
                mapBegin.put(attendance.getClockingTime().toLocalDate().toString(),attendance);
            }else {
                mapFinish.put(attendance.getClockingTime().toLocalDate().toString(),attendance);
            }
        }
        for (var entry : mapBegin.entrySet()) {
            Attendance attendanceFinish = mapFinish.get(entry.getKey());
            Attendance attendanceBegin = entry.getValue();
            Long timeWorkInOneDay = (Duration.between(attendanceBegin.getClockingTime(), attendanceFinish.getClockingTime()).toMinutes() - breakTime);
            if(timeWorkInOneDay>=30){
            totalWorkDuration +=timeWorkInOneDay;
            }
        }
        return totalWorkDuration;
    }

    public AttendanceCalculate calculate(FormDate formDate){
      Long workDays =  countWorkDay(formDate);
      Long workTime =  attendanceAnalysis(formDate.getStartDate(),formDate.getEndDate());

      AttendanceCalculate attendanceCalculate= new AttendanceCalculate();
      attendanceCalculate.setWorkDays(workDays);
      attendanceCalculate.setHoliday(6L);
      attendanceCalculate.setWorkTime(workTime);
      attendanceCalculate.setOverTime(600L);
      attendanceCalculate.setBreakTime(900L);
        return attendanceCalculate;
    }
}
