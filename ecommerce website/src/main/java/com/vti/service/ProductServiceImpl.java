package com.vti.service;

import com.vti.entity.Product;
import com.vti.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {

    @Autowired
    private ProductRepository productRepository;

    @Override
    public List<Product> getAllProducts() {
        List<Product> products = productRepository.findAll();
        return products;
    }

    @Override
    public Product getProductById(Integer id) {
        Product productGetById = productRepository.findById(id).get();
        return productGetById;
    }

    @Override
    public Product save(Product product) {
        Product productNew = productRepository.save(product);
        return productNew;
    }

    @Override
    public Product updateProduct(Product product) {
        Product productUpdate = productRepository.save(product);
        return productUpdate;
    }

    @Override
    public boolean deleteProductById(Integer id) {
        Product product = productRepository.getById(id);
        productRepository.delete(product);
        return true;
    }

    @Override
    public List<Product> searchProductByName(String productName) {
        List<Product> productListSearchByName = productRepository.searchProductByName(productName);
        return productListSearchByName;
    }
}
