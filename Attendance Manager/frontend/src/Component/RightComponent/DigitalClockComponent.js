import React from "react";
import { useState } from "react";
import './DigitalClockComponent.css';

const DigitalClockComponent = () =>{
    let time = new Date().toLocaleTimeString();
    let date = new Date().toLocaleDateString();

    const [ctime, setTime] = useState(time);
    const [ cdate, setDate] = useState(date);
    const UpdateTime = ()=>{
        time = new Date().toLocaleTimeString();
        setTime(time);
    }
    const UpdateDate = () =>{
        date = new Date().toLocaleDateString('ja',{year:'numeric',month:'long',day:'numeric'});
        setDate(date);
    }
    setInterval(UpdateTime);
    setInterval(UpdateDate);
    return(
        <div className="container-digital-clock">
            <div className="item-time"> 
                <text className="digitalClock-text">{ctime}</text>
            </div>
            <div className="item-date">
                <text className="date-text">{cdate}</text>
            </div>
        </div>
    )
}
export default DigitalClockComponent;