import './App.css';
import React,{useState} from 'react';
import RightComponent from './Component/RightComponent/RightComponent';
import SidebarComponent from './Component/SidebarComponent';
import CenterComponent from './Component/CenterComponent';
import { format } from 'date-fns';

const App = (props) => {

const[date, setDate] = useState(new Date())
const dateCallbackFunction = (newDate) => {
   setDate(newDate) 
 }
 const dateFormat = new Date(date);
 const firstDay = new Date(dateFormat.getFullYear(), dateFormat.getMonth(), 1).toDateString();
 const lastDay = new Date(dateFormat.getFullYear(),dateFormat.getMonth() + 1, 0).toDateString();
 const startDate = format(new Date(firstDay),'yyyy-MM-dd');
 const endDate = format(new Date(lastDay),'yyyy-MM-dd')
 console.log(startDate,endDate)


  return (
    <div className='container'>
    <div className='item-sidebar'>
      <SidebarComponent/>
   </div>
   <div className='item-center'>
   <CenterComponent startDate={startDate} endDate={endDate} />
   </div>
   <div className='item-right'>
      <RightComponent dateCallback={dateCallbackFunction} />
   </div>
  </div>
  );
}

export default App;
