package com.rikkei.repository;
import com.rikkei.entity.Attendance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;


@Repository
public interface AttendanceRepository extends JpaRepository<Attendance, Long> {


    Page<Attendance> findAll(Pageable pageable);

    @Query(value = "SELECT COUNT( DISTINCT date_format(clocking_time,'%y/%m/%d') ) FROM attendance  WHERE clocking_time >= :startDate AND clocking_time < DATE_ADD(:endDate, INTERVAL 1 DAY) AND attendance_status = 'finish'", nativeQuery = true)
    Long countAttendancesByClockingTime(LocalDate startDate, LocalDate endDate);

    @Query(value = "SELECT * FROM attendance  WHERE clocking_time >= :startDate AND clocking_time <  DATE_ADD(:endDate, INTERVAL 1 DAY)", nativeQuery = true)
    List<Attendance> findAttendancesByClockingTime( LocalDate startDate, LocalDate endDate);

}
