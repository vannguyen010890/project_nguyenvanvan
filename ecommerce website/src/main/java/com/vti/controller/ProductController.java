package com.vti.controller;

import com.vti.dto.ProductDTO;
import com.vti.entity.Category;
import com.vti.entity.Product;
import com.vti.service.CategoryService;
import com.vti.service.ProductService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("product")
public class ProductController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;


    @GetMapping
    public String getAllProducts(Model model){
        List<Product> entities = productService.getAllProducts();
        List<ProductDTO> productDTOS = modelMapper.map(entities, new TypeToken<List<ProductDTO>>(){}.getType());
        model.addAttribute("listProduct",productDTOS);
        return "product-list";
    }

    @GetMapping(value = "/{id}")
    public ProductDTO getProductById(@PathVariable(name = "id") Integer id){
        Product entity = productService.getProductById(id);
        ProductDTO productDTO = modelMapper.map(entity, ProductDTO.class);
        return productDTO;
    }

    @GetMapping("create")
    public String create(){
        return "product-create";
    }

    @PostMapping("createProduct")
    public String createProduct(@ModelAttribute("product") Product product, @RequestParam("categoryId") String categoryId){
        Category category = categoryService.getCategoryById(Integer.parseInt(categoryId));
        product.setCategory(category);
        productService.save(product);
        return "redirect:/product";
    }

    @GetMapping("update/{id}")
    public String update(Model model, @PathVariable("id") Integer id){
        Product product = productService.getProductById(id);
        ProductDTO productDTO = modelMapper.map(product, ProductDTO.class);
        Category category = categoryService.getCategoryById(product.getCategory().getId());
        model.addAttribute("product2",productDTO);
        model.addAttribute("category",category);
        return "product-update";
    }

    @PostMapping("updateProduct")
    public String updateProduct(@ModelAttribute("product") Product product, @RequestParam("categoryId") Integer categoryId){
        Category category = categoryService.getCategoryById(categoryId);
        product.setCategory(category);
        productService.save(product);
        return "redirect:/product";
    }

    @GetMapping("delete/{id}")
    public String delete(@PathVariable("id") Integer id){
        productService.deleteProductById(id);
        return "redirect:/product";
    }

    @GetMapping("search")
    public String searchProductByName(Model model,@RequestParam("productName") String productName){
       List<Product> productListSearchByName= productService.searchProductByName(productName);
        List<ProductDTO> productDTOS = modelMapper.map(productListSearchByName, new TypeToken<List<ProductDTO>>(){}.getType());
       model.addAttribute("listProduct",productDTOS);
       return "product-list";
    }
}
