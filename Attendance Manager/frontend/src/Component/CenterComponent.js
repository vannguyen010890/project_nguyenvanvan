import React,{useState} from "react";
import ActionComponent from "./ActionComponent";
import GreetingComponent from "./GreetingComponent";
import ListRecentActivities from "./ListRecentActivities";
import GetAttendanceCalculate from "./GetAttendanceCalculate";
import './CenterComponent.css';

const CenterComponent = ({startDate, endDate}) =>{

    const [attendance, setAttendance] = useState({
        clokingTime:'',
        status:''
    })
    const attendanceCallbackFunction = (attendance) =>{
        console.log(attendance)
        setAttendance({attendance})
    }
    
    return(
        <div className="container-center">
            <div className="greeting">
                <GreetingComponent/>
            </div>
            <div className="action">
                <ActionComponent  attendanceCallback={attendanceCallbackFunction}/>
            </div>
            <div className="listRecentActivities">
                <ListRecentActivities attendance = {attendance}/>
            </div>
            <div className="calculate">
                <GetAttendanceCalculate attendance = {attendance} startDate={startDate} endDate ={endDate} />
            </div>
        </div>
    )
}

export default CenterComponent;