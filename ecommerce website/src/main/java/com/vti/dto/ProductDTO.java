package com.vti.dto;

import com.vti.entity.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductDTO {

    private String productName;

    private Integer id;

    private String price;

    private String quantity;

    private String categoryName;
}
