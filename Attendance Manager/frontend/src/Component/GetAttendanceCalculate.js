import React,{useEffect, useState} from "react";
import './GetAttendanceCalculate.css';
import attentionImg from'./MenuImages/attention_img.svg';
import axios from "axios";


const GetAttendanceCalculate = ({startDate, endDate,attendance}) =>{
    
  console.log(attendance)

    const[attendanceCalculate, setAttendanceCalculate] = useState({
        workDays : "",
        holiday:"",
        workTime :"",
        overTime:"",
        breakTime:""
    }) 
      useEffect(() => {
       const url="http://localhost:8082/api/v1/attendance/analysis";
              axios.post(url,{
                startDate,
                endDate  
            })
            .then(res=>{
                setAttendanceCalculate({
                    workDays:res.data.workDays,
                    holiday:res.data.holiday,
                    workTime:res.data.workTime,
                    overTime:res.data.overTime,
                    breakTime:res.data.breakTime
                });
                console.log(res.data)
              }, [])
            .catch(error =>{
                console.log(error)
            })
          },[startDate,attendance])

    return(
        <div>
            <h2 className="calculate-title">退勤統計</h2>
            <div className="container-calculate-attendance">
            <div className="item-work-day"><text className="item-title">出勤日</text><br></br><text className="item-calculate"> {attendanceCalculate.workDays}日</text></div>
            <div className="item-holiday"><text className="item-title">土日祝</text><br></br><text className="item-calculate"> {attendanceCalculate.holiday}日</text></div>
            <div className="item-work-time">
                <div>
                    <div className="item-title">出勤時間</div>
                    <div className="item-detail">
                        <div className="attention-img"><img className="attentionmg" src={attentionImg} alt="" /></div>
                        <div className="item-detail-title">詳細</div>
                    </div>
                </div>
                <div>
                    <text className="item-calculate-work-time">  {Math.floor(attendanceCalculate.workTime/60)}時{(attendanceCalculate.workTime % 60)}分</text>
                </div>    
            </div>
            <div className="item-overtime"><text className="item-title">残業時間</text><br></br><text className="item-calculate">  {Math.floor(attendanceCalculate.overTime/60)}時</text></div>
            <div className="item-break-time"><text className="item-title">休憩時間</text><br></br><text className="item-calculate">  {Math.floor(attendanceCalculate.breakTime/60)}時</text></div>
         </div>
        </div>
    )
}

export default GetAttendanceCalculate;