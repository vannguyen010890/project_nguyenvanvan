package com.rikkei.formRequest;

import com.sun.istack.NotNull;
import lombok.Data;

import java.time.LocalDate;

@Data
public class FormDate {

    @NotNull
    LocalDate startDate;

    @NotNull
    LocalDate endDate;
}
