import React from "react";
import './SidebarComponent.css';
import homeImg from './MenuImages/home.svg';
import userImg from './MenuImages/user.svg';
import timeSheetImg from './MenuImages/timeSheet.svg';
import settingImg from './MenuImages/setting.svg';
import logoutImg from'./MenuImages/logout.svg';
import RectangleImg from'./MenuImages/Rectangle.svg';



const SidebarComponent = () =>{
    

    return(
        <div className="container-sidebar">
            <div className="logo">
                <h3 className="logo-name"> AM</h3>
                <text className="company-name">RIKKEI</text>
            </div>
            <div className="home-container">
                <div className="rectangle">
                    <img className="rectangle-img" src={RectangleImg} alt=""/>
                </div>
                <div className="home">
                    <img className="home-img" src={homeImg} alt=""/>
                    <h6 className="text-home">ホーム</h6>
                </div>
            </div>
            <div className="user">
                <img className="user-img" src={userImg} alt="" />
                <h6 className="user-text">ユーザ</h6>
            </div>
            <div className="timeSheet">
                <img className="timeSheet-img" src={timeSheetImg} alt="" />
                <h6 className="timeSheet-text">タイムシート</h6>
            </div>
            <div className="setting">
                <img className="setting-img" src={settingImg} alt="" />
                <h6 className="setting-text">設定</h6>
            </div>
            <div className="logout">
                <img className="logout-img" src={logoutImg} alt="" />
                <h6 className="logout-text">ログアウト</h6>
            </div>    
        </div>
    )
}

export default SidebarComponent;