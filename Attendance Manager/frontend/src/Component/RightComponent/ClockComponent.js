import React from "react";
import AnalogClockComponent from "./AnalogClockComponent";
import'./ClockComponent.css';
import DigitalClockComponent from "./DigitalClockComponent";

const ClockComponent = () =>{
    return(
        <div className="container-clock">
            <div className="item-analog-clock">
                <AnalogClockComponent/>
            </div>
            <div className="item-digital-clock">
                <DigitalClockComponent/>
            </div>
        </div>
    )
}
export default ClockComponent;