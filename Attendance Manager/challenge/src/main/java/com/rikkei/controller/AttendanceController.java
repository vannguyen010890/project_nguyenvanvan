package com.rikkei.controller;

import com.rikkei.entity.Attendance;
import com.rikkei.entity.AttendanceCalculate;
import com.rikkei.formRequest.FormDate;
import com.rikkei.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/attendance")
public class AttendanceController {

    @Autowired
    private AttendanceService attendanceService;

    @GetMapping("pageList")
    public Page<Attendance> findAll(Pageable pageable) {
        return attendanceService.findAll(pageable);
    }

    @PostMapping("add_attendance")
    public Attendance addAttendance(@RequestBody Attendance attendance) {
        return attendanceService.addAttendance(attendance);
    }

    @GetMapping("findAttendance")
    public List<Attendance> findAttendancesByClockingTime(@RequestParam("startDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)  LocalDate startDate, @RequestParam("endDate") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate endDate) {
        return attendanceService.findAttendancesByClockingTime(startDate,endDate);

    }

    @PostMapping("countWorkDays")
    public Long countWorkDay(@RequestBody FormDate formDate){
        return attendanceService.countWorkDay(formDate);
    }

    @PostMapping("analysis")
    public AttendanceCalculate calculate(@RequestBody FormDate formDate){
        return attendanceService.calculate(formDate);
    }

}
