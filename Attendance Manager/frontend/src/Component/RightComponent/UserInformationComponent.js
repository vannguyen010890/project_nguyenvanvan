import React from "react";
import './UserInformationComponent.css';
import UserAvatarImg from './UserInformationImages/avatar.svg'
import notificationImg from './UserInformationImages/notification.svg'
import memberImg from  './UserInformationImages/member.svg'
import computerImg from  './UserInformationImages/computer.svg'
import locationImg from  './UserInformationImages/location.svg'
import attentionImg from  './UserInformationImages/attention.svg'

const UserInformationCompoent = () =>{
    return(
        <div className="container-userInformation">
            <div className="item-avatar">
                <img className="avatar-img" src={UserAvatarImg} alt=""/>
            </div>
            <div className="item-user-container">
                <div className="user-name">
                    <h3 className="userName-text"> VO DAI TRINH</h3>
                </div>
                <div className="information">
                    <div className="item-information-1">
                        <img className="member-img" src={memberImg} alt=""/>
                        <text className="userPosition-text"> メンバー</text>
                    </div>
                    <div className="item-information-2">
                        <img className="computer-img" src={computerImg} alt=""/>
                        <text className="userDivice-text">会社のPC</text>
                    </div>
                     <div className="item-information-3">
                        <img className="location-img" src={locationImg} alt=""/>
                        <text className="userLocation-text"> 会社</text>
                        <img className="attention-img" src={attentionImg} alt=""/>
                </div>
            </div>
            </div>
            <div className="item-notification">
                <img className="notification-img" src={notificationImg} alt=""/>
            </div>
        </div>
    )
}
export default UserInformationCompoent;