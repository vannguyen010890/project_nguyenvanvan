import React from "react";
import './GreetingComponent.css';


const GreetingComponent = () => {
    let timeOfDay;
    const date = new Date();
    const hours = date.getHours();

    if(hours >= 0 && hours < 12){
        timeOfDay = 'おはようございます';
    }else if (hours >= 12 && hours < 17) {
        timeOfDay = 'こんにちは';
    }else {
        timeOfDay = '今晩は';
    }
    return(
        <div>
            <div className="greeting">
                <text className="greeting-text">{timeOfDay}, VO DAI TRINH</text>
            </div>
        </div>
    )
}
export default GreetingComponent;