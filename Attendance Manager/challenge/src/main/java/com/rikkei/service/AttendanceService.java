package com.rikkei.service;
import com.rikkei.entity.Attendance;
import com.rikkei.entity.AttendanceCalculate;
import com.rikkei.formRequest.FormDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDate;
import java.util.List;

public interface AttendanceService {

    List<Attendance> getAttendances();

    Attendance getById(Long id);

    Page<Attendance> findAll(Pageable pageable);

    Attendance addAttendance(Attendance attendance);

    Long countWorkDay(FormDate formDate);

    List<Attendance> findAttendancesByClockingTime(LocalDate startDate, LocalDate endDate);

    Long attendanceAnalysis(LocalDate startDate, LocalDate endDate);

    public AttendanceCalculate calculate(FormDate formDate);

}
