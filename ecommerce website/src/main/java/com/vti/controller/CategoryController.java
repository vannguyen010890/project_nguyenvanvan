package com.vti.controller;


import com.vti.dto.CategoryDTO;
import com.vti.entity.Category;
import com.vti.service.CategoryService;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("category")
public class CategoryController {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private CategoryService categoryService;

    @GetMapping
    public String getAllCategories(Model model){
        List<Category> entities2 = categoryService.getAllCategories();
        List<CategoryDTO> categoryDTOS = modelMapper.map(entities2, new TypeToken<List<CategoryDTO>>(){}.getType());
        model.addAttribute("listCategory", categoryDTOS);
        return "category-list";
    }

    @GetMapping(value = "/{id}")
    public CategoryDTO getCategoryById(@PathVariable(name = "id") Integer id){
        Category entity2 = categoryService.getCategoryById(id);
        CategoryDTO categoryDTO = modelMapper.map(entity2,CategoryDTO.class);
        return categoryDTO;
    }
    @GetMapping("create")
    public String create(){
        return "category-create";
    }

    @PostMapping("createCategory")
    public String createCategory(@ModelAttribute("category") Category category){
        categoryService.save(category);
        return "redirect:/category";
    }

    @GetMapping("update/{id}")
    public String update(Model model,@PathVariable("id") Integer id){
        Category category = categoryService.getCategoryById(id);
        model.addAttribute("category2", category);
        return "category-update";
    }

    @PostMapping("updateCategory")
    public String updateCategory(@ModelAttribute("category") Category category){
        categoryService.save(category);
        return "redirect:/category";
    }

    @GetMapping("delete/{id}")
    public String deleteCategory(@PathVariable("id") Integer id){
        categoryService.deleteCategoryById(id);
        return "redirect:/category";
    }

    @GetMapping("search")
    public String searchCategoryByName(Model model, @RequestParam("categoryName") String categoryName){
        List<Category> categoryListSearchByName = categoryService.searchCategoryByName(categoryName);
        List<CategoryDTO> categoryDTOS = modelMapper.map(categoryListSearchByName, new TypeToken<List<CategoryDTO>>(){}.getType());
        model.addAttribute("listCategory", categoryDTOS);
        return "category-list";
    }

}
