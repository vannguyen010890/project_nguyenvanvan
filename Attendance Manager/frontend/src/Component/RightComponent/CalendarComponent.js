import React from "react";
import { Calendar } from "react-calendar";
import 'react-calendar/dist/Calendar.css';
import'./CalendarComponent.css';
import todayImg from  './CalendarImages/today1.svg'
import CalendarTodayComponent from "./CalendarTodayComponent";

function CalendarComponent(props) {
 
    return (
      <div className="calendar">
      <h3 className="calendar-title">カレンダー</h3>
      <div className="calendar-container">
            <Calendar
            next2Label={null}
            prev2Label={null}
            formatMonthYear = {
              (locale, date) => new Intl.DateTimeFormat(
                locale, 
                {
                  year: "numeric", 
                  month: "2-digit", 
                }).format(date)
              }
              formatDay = {
                (locale, date) => new Intl.DateTimeFormat(
                  'fr-CA', 
                  {
                    day: "2-digit"
                  }).format(date)
                }
              
              tileContent={
                ({ date, view }) => {
                  return view === 'month' && date.getMonth() === new Date().getMonth() && date.getDate() === new Date().getDate() 
                  ? <CalendarTodayComponent/>
                  // <img className="today-img" src={todayImg} alt=""/>
                  // &&
                  // <text className="today-text">今日</text>
                  // ? <img className="today-img" src={todayImg} alt=""/>
                  // ? <p  className="display-today">今日</p> 
                  : null
                }
              }
              
            onActiveStartDateChange = {({ activeStartDate}) =>
            {
              console.log(activeStartDate)
              const {dateCallback} = props
              dateCallback(activeStartDate)   
            }    
          } 
                 />
                 </div>
        </div>
    );
   
};
export default CalendarComponent;