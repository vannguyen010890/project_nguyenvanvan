package com.vti.service;

import com.vti.entity.Product;

import java.util.List;

public interface ProductService {

    List<Product> getAllProducts();

    Product getProductById(Integer id);

    Product save(Product product);

    Product updateProduct(Product product);

    boolean deleteProductById(Integer id);

    List<Product> searchProductByName(String productName);
}
