package com.vti.service;

import com.vti.entity.Category;

import java.util.List;

public interface CategoryService {

    List<Category> getAllCategories();

    Category getCategoryById(Integer id);

    Category save(Category category);

    Category updateCategory(Category category);

    boolean deleteCategoryById(Integer id);

    List<Category> searchCategoryByName(String categoryName);
}
