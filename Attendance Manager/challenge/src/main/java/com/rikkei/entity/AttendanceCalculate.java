package com.rikkei.entity;

import lombok.Data;

@Data
public class AttendanceCalculate {

    Long workDays;

    Long holiday;

    Long workTime;

    Long overTime;

    Long breakTime;
}
