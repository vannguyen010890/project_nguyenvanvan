import AnalogClock from 'analog-clock-react';
   
export default function ReactClock() {
  let options = {
    width: "130px",
    border: true,
    borderColor: "#ECEEF3",
    baseColor: "#F1F2F7",
    centerColor: "none",
    
    centerBorderColor: "none",
    handColors: {
      second: "#FD251E",
      minute: "#646E82",
      hour: "#646E82"
    }
  };
   
  return (
    <div>
      <AnalogClock {...options} />
    </div>
  )
}