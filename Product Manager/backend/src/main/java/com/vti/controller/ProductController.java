package com.vti.controller;

import com.vti.exception.ResourceNotFoundException;
import com.vti.model.Product;
import com.vti.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/products")
public class ProductController {

    @Autowired
    private ProductRepository productRepository;

    @GetMapping
    public List<Product> getAllProducts(){
        return productRepository.findAll();
    }
    //build create Product REST API
    @PostMapping
    public Product createProduct(@RequestBody Product product){
        return productRepository.save(product);
    }
    //get Product By Id
    @GetMapping("{id}")
    public ResponseEntity<Product> getProductById(@PathVariable Long id){
        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product is not exist with id: " + id));
        return ResponseEntity.ok(product);
    }

    //update Product Rest api
    @PutMapping("{id}")
    public  ResponseEntity<Product> updateProduct(@PathVariable long id, @RequestBody Product productDetails){
        Product updateProduct = productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(("Product is not exist with id: " + id)));

        updateProduct.setProductName(productDetails.getProductName());
        updateProduct.setDescription(productDetails.getDescription());
        updateProduct.setPrice(productDetails.getPrice());

        productRepository.save(updateProduct);

        return ResponseEntity.ok(updateProduct);
    }

    //delete Product Rest api
    @DeleteMapping("{id}")
    public ResponseEntity<HttpStatus> deleteProduct(@PathVariable Long id){

        Product product = productRepository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException("Product is not exist with id : " + id));

        productRepository.delete(product);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
