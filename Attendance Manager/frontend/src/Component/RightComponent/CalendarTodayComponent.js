import React from "react";
import todayImg from  './CalendarImages/today1.svg'
import'./CalendarTodayComponent.css';

const CalendarTodayComponent =() =>{
    return(
        <div>
            <img className="today-img" src={todayImg} alt=""/>
            <p className="today-text">今日</p>
        </div>
    )
}
export default CalendarTodayComponent;