import React from "react";
import CalendarComponent from "./CalendarComponent";
import ClockComponent from "./ClockComponent";
import './RightComponent.css';
import UserInformationCompoent from "./UserInformationComponent";


const RightComponent = (props) =>{

    return(
        <div className="container-right">
            <div className="item-user-information">
                <UserInformationCompoent/>
            </div>
            <div className="item-clock">
                <ClockComponent/>
            </div>
            <div className="item-calendar">
                <CalendarComponent dateCallback={props.dateCallback} />
            </div>
        </div>
    )
}
export default RightComponent;