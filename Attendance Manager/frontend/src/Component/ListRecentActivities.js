import React, { useEffect, useState } from "react";
import './ListRecentActivities.css';
import AttendanceService from '../Service/AttendanceService';

const ListRecentActivities = (attendance) => {
    const [ attendances, setAttendances] = useState([])
    useEffect(() => {
        getRecentActivities();
    },[attendance]);

    const getRecentActivities = () => {
        AttendanceService.getRecentActivities()
        .then((respone) => {
            setAttendances(respone.data.content);
            console.log(respone.data)
        }).catch(error => {
            console.log(error);
        })
    }
    const button = (attendanceStatus) => {
        if(attendanceStatus === "begin"){
            return <button className ="button-attendance" >出勤</button>
        } else {
            return  <button className = "button-leave" >退勤</button>
        }
    }
    var WeekChars = [ "(日)", "(月)", "(火)", "(水)", "(木)", "(金)", "(土)" ];
    
    return(
        <div className="list-recent-activities">
            <h2 className="recent-activities-title">最近の活動</h2>
            <div>
            <div>
            {
                attendances.map(
                    attendance =>
                    <div className="attendance-list" key = {attendance.id}>
                    <div className="container-recent-activities">
                            <div className="text-date-time">{new Date(attendance.clockingTime).getFullYear()}年{new Date(attendance.clockingTime).getMonth()+1}月{new Date(attendance.clockingTime).getDate()}日  {WeekChars[new Date(attendance.clockingTime).getDay()]}</div>
                        　　 <div className="text-time">{new Date(attendance.clockingTime).getHours()}時{new Date(attendance.clockingTime).getMinutes().toString().replace(/^(\d)$/, '0$1')}分{new Date(attendance.clockingTime).getSeconds().toString().replace(/^(\d)$/, '0$1')}秒</div>
                            <div className="button-in-list-recent-activities" > { button(attendance.attendanceStatus)} </div>
                        </div>
                    </div>
                )}
            </div>
            </div>

        </div>
    )
}

export default ListRecentActivities;