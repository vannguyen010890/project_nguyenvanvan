import React,{useState,useEffect} from "react";
import axios from "axios";
import moment from "moment";
import holidayImg from './ActionImages/holiday.svg';
import attendanceImg from './ActionImages/attendance.svg';
import overtimeImg from './ActionImages/overtime.svg';
import LeaveImg from './ActionImages/Leaving.svg';
import './ActionComponent.css';



const ActionComponent = (props) =>{

    const [attendance, setAttendance] = useState({
        clockingTime:  "",
        attendanceStatus: ""
    });
    
    useEffect(() => {
    const url="http://localhost:8082/api/v1/attendance/add_attendance"
      axios.post(url,attendance) 
      .then(()=>{
        const {attendanceCallback} = props
        attendanceCallback(attendance)
      })  
    .catch(error =>{
        console.log(error)
    })
    },[attendance])

    const [beginClicked, setBeginClicked] = useState(false);
    const [finishClicked, setFinishClicked] = useState(false);
    const [showError, setShowError] = useState(false);
    const [showError2, setShowError2] = useState(false);
    return(
        <div className="container-action">
            <div className="item-action">
                <div className="item-action-container">
                    <div className="item-circle">
                        <svg className="attendance-svg" viewBox="0 0 80 80" width="20" height="20">
                        <circle class="circle-attendance" cx="40" cy="40" r="38"/>
                        </svg>
                    </div>
                    <div className="item-img">
                        <img className="attendance-img" src={attendanceImg} alt=""/>
                    </div>
                    <div className="item-button">
                         <button onClick={() =>{
                        if(!beginClicked){
                            setAttendance({
                                clockingTime:moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
                                attendanceStatus: "begin"
                         })
                            setBeginClicked(true);
                            setFinishClicked(false);
                        }else{
                            setShowError(true)
                            setTimeout(()=>{
                                setShowError(false)
                            },2000);
                        }}
                    }
                             className="action-button">出勤</button>
                    </div>
                        {showError && (
                           <p className="error-alert">
                                    Bạn vẫn chưa 退勤！
                            </p> 
                        )}
                </div>
            </div>
            <div className="item-action">
                <div className="item-action-container">
                    <div className="item-circle" >
                        <svg className="leave-svg" viewBox="0 0 80 80" width="20" height="20">
                            <circle class="circle-leave" cx="40" cy="40" r="38"/>
                        </svg>
                    </div>
                    <div className="item-img">
                        <img className="leave-img" src={LeaveImg} alt=""/>
                    </div>
                    <div className="item-button">
                        <button onClick={() =>{
                            if(beginClicked && !finishClicked){     
                        setAttendance({
                            clockingTime:moment(new Date()).format("YYYY-MM-DDTHH:mm:ss"),
                            attendanceStatus: "finish"
                         })
                         setFinishClicked(true);
                         setBeginClicked(false);
                        }else{
                            setShowError2(true);
                            setTimeout(()=>{
                                setShowError2(false);
                            },2000);
                        }
                    }  }
                            className="action-button">退勤</button>
                    </div>
                    </div>
                            {showError2 && (
                                <div className="error-alert-2">
                                    Bạn vẫn chưa 出勤！
                                </div>
                            )}
                 </div>
            
            <div className="item-action">
                <div className="item-action-container">
                    <div className="item-circle" >
                        <svg className="overtime-svg" viewBox="0 0 80 80" width="20" height="20">
                            <circle class="circle-overtime" cx="40" cy="40" r="38"/>
                        </svg>
                     </div>
                    <div className="item-img">
                         <img className="overtime-img" src={overtimeImg} alt=""/>
                    </div>
                    <div className="item-button">
                        <button className="action-button">残業</button>
                     </div>
                </div>
            </div>
            <div className="item-action">
                <div className="item-action-container">
                    <div className="item-circle" >
                        <svg className="holiday-svg" viewBox="0 0 80 80" width="20" height="20">
                            <circle class="circle-holiday" cx="40" cy="40" r="38"/>
                        </svg>
                    </div>
                    <div className="item-img">
                        <img className="holiday-img" src={holidayImg} alt=""/>
                    </div>
                    <div className="item-button">
                        <button className="action-button">休暇</button>
                    </div>
                </div>
            </div>
        </div>
  );
}
export default ActionComponent;