package com.vti.service;

import com.vti.entity.Category;
import com.vti.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService{

    @Autowired
    private CategoryRepository categoryRepository;

    @Override
    public List<Category> getAllCategories() {
        List<Category> categories = categoryRepository.findAll();
        return categories;
    }

    @Override
    public Category getCategoryById(Integer id) {
        Category categoryGetById = categoryRepository.findById(id).get();
        return categoryGetById;
    }

    @Override
    public Category save(Category category) {
        Category categoryNew = categoryRepository.save(category);
        return categoryNew;
    }

    @Override
    public Category updateCategory(Category category) {
        Category categoryUpdate = categoryRepository.save(category);
        return categoryUpdate;
    }

    @Override
    public boolean deleteCategoryById(Integer id) {
        Category category = categoryRepository.findById(id).get();
        categoryRepository.delete(category);
        return true;
    }

    @Override
    public List<Category> searchCategoryByName(String categoryName) {
        List<Category> categoryListSearchByName = categoryRepository.searchCategoryByName(categoryName);
        return categoryListSearchByName;
    }


}
